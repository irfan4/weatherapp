//
//  WeatherAppTests.swift
//  WeatherAppTests
//
//  Created by Irfan Bashir on 11/17/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import XCTest
@testable import WeatherApp

class WeatherAppTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testNSObjectDelayExtension() {
        let promise = expectation(description: "Delay Function")
        var testIsFailing = true
        self.delay(0.365) {
            testIsFailing = false
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 1, handler: nil)
        XCTAssertEqual(testIsFailing, false)
    }
    
    func testArrayRemoveIndexesExtension() {
        var array: [String] = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
        
        array.remove(at: [0, 2, 4, 6, 8])
        
        XCTAssertTrue(array.joined(separator: "") == "246810" )
    }
    
    func testImageCacheManagerShouldReturnValidImage() {
        let promise = expectation(description: "Image Download Function")
        var image: UIImage?
        
        WAImageCacheManager.shared.downloadImage(url: URL(string: "https://cdn.apixu.com/weather/64x64/night/116.png")!) { (img, error) in
            image = img
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssert(image != nil, "Downloaded")
    }

}
