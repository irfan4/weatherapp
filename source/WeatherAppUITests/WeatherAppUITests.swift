//
//  WeatherAppUITests.swift
//  WeatherAppUITests
//
//  Created by Irfan Bashir on 11/17/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import XCTest
@testable import WeatherApp

class WeatherAppUITests: XCTestCase {

    var controllerUnderTest: WARootViewController!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSearchViewControllerLaunch() {
        let app = XCUIApplication()
        app.navigationBars.buttons["Add"].tap()
        XCTAssert(app.searchFields.count > 0)
    }

    func testSearchViewControllerSearch() {
        let app = XCUIApplication()
        app.navigationBars.buttons["Add"].tap()
        app.searchFields["Enter city name"].typeText("Dubai")
    }
}
