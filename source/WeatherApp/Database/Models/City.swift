//
//  City.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 2019-12-19.
//  Copyright © 2019 Hamda, Inc. All rights reserved.
//

import Foundation
import EVReflection

class City: EVObject {
    var id: Int32 = 0
    var name: String = ""
    var country: String = ""
    var lon: Double = 0.0
    var lat: Double = 0.0
    
    static var Keys: [String] {
        return ["id", "name", "country", "lon", "lat"]
    }
}
