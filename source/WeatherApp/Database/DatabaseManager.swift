//
//  DatabaseManager.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 2019-12-19.
//  Copyright © 2019 Hamda, Inc. All rights reserved.
//

import UIKit
import CoreData
import EVReflection

typealias DBSearchCompletionHandler = (_ cities: [City]) -> Void

class DatabaseManager: NSObject {
    static let shared: DatabaseManager = DatabaseManager()
    private let queue = OperationQueue()
    private let loadOperation = LoadOperation()
    private var dbReady = false
    private var lastCompletion: DBSearchCompletionHandler?
    private var lastSearch: String = ""
    
    private override init() {
        super.init()
        
        dbReady = UserDefaults.standard.bool(forKey: "DBReady")
        
        if !self.dbReady {
            
            loadOperation.managedContext = persistentContainer.viewContext
            queue.qualityOfService = .userInitiated
            queue.maxConcurrentOperationCount = 1
            queue.addOperation(loadOperation)
            queue.addOperation { [weak self] in
                self?.dbReady = true
                if let strongSelf = self, let completionHandler = strongSelf.lastCompletion {
                    strongSelf.search(keyword: strongSelf.lastSearch, completion: completionHandler)
                }
            }
        }
    }
        
    deinit {
        self.saveContext()
        queue.isSuspended = true
        queue.cancelAllOperations()
    }
    
    func getCity(withId id: Int32) -> City? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CityMO")
        fetchRequest.predicate = NSPredicate(format: "id == %d", id)
        fetchRequest.fetchLimit = 1
        do {
            if let result = try persistentContainer.viewContext.fetch(fetchRequest) as? [NSManagedObject] {
                let cities = result.map { City(dictionary: $0.dictionaryWithValues(forKeys: City.Keys) as NSDictionary)}
                return cities.first
            }
        } catch let error {
            print(error)
        }
        return nil
    }
    
    func remove(forecast: DarkSkyForeCast) {
        let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateMOC.parent = persistentContainer.viewContext
        privateMOC.perform { [weak self] in
            
            let _ = self
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WeatherMO")
            fetchRequest.predicate = NSPredicate(format: "cityId = %d", forecast.cityId)
            
            do {
                if let result = try privateMOC.fetch(fetchRequest) as? [NSManagedObject] {
                    for item in result {
                        privateMOC.delete(item)
                    }
                }
                try privateMOC.save()
                try privateMOC.parent?.save()
            } catch let error {
                print(error)
            }
        }
    }
    
    func add(city: City, initialForecast forecast: DarkSkyForeCast) {
        let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateMOC.parent = persistentContainer.viewContext
        privateMOC.perform { [weak self] in
            
            let _ = self
            
            do {
                let weatherEntity = NSEntityDescription.entity(forEntityName: "WeatherMO", in: privateMOC)!
                let weather = NSManagedObject(entity: weatherEntity, insertInto: privateMOC)
                weather.setValue(city.id, forKeyPath: "cityId")
                weather.setValue(forecast.currently.apparentTemperature, forKeyPath: "feelsLike")
                weather.setValue(forecast.currently.icon, forKeyPath: "icon")
                weather.setValue(forecast.currently.summary, forKeyPath: "summary")
                weather.setValue(forecast.currently.temperature, forKeyPath: "temperature")
                weather.setValue(forecast.timezone, forKeyPath: "timezone")
                weather.setValue(forecast.currently.windSpeed, forKeyPath: "windSpeed")
                weather.setValue(forecast.latitude, forKeyPath: "latitude")
                weather.setValue(forecast.longitude, forKeyPath: "longitude")
                weather.setValue(city.name, forKeyPath: "location")
                try privateMOC.save()
                try privateMOC.parent?.save()
            } catch {
                // handle error
            }
            
        }
        
    }
    
    func search(keyword: String,
                completion: @escaping DBSearchCompletionHandler) {
        lastSearch = keyword
        if !dbReady {
            // Database is still upgrading
            lastCompletion = completion
            completion([])
            return
        }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CityMO")
        fetchRequest.predicate = NSCompoundPredicate(type: .or, subpredicates: [NSPredicate(format: "name contains[c] %@", keyword), NSPredicate(format: "country contains[c] %@", keyword)])
        fetchRequest.fetchLimit = 30
        do {
            if let result = try persistentContainer.viewContext.fetch(fetchRequest) as? [NSManagedObject] {
                let cities = result.map { City(dictionary: $0.dictionaryWithValues(forKeys: City.Keys) as NSDictionary)}
                completion(cities)
                return
            }
        } catch let error {
            print(error)
        }
        completion([])
    }
    
    func my() -> [DarkSkyForeCast] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WeatherMO")
        var items = [DarkSkyForeCast]()
        do {
            if let results = try persistentContainer.viewContext.fetch(fetchRequest) as? [NSManagedObject] {
                for result in results {
                    if let cityId = result.value(forKeyPath: "cityId") as? Int32,
                        let name = result.value(forKeyPath: "location") as? String,
                    let lat = result.value(forKey: "latitude") as? Double,
                    let lon = result.value(forKey: "longitude") as? Double
                    {
                        let item = DarkSkyForeCast(fileNameInTemp: "\(cityId).dat")
                        item.cityId = cityId
                        item.location = name
                        item.latitude = lat
                        item.longitude = lon
                        items.append(item)
                    }
                }
            }
        } catch let error {
            print(error)
        }
        return items
    }
    
    var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    class LoadOperation: Operation {
        var managedContext: NSManagedObjectContext!
        
        override var isAsynchronous: Bool {
            return true
        }
        var _isFinished: Bool = false
        
        override var isFinished: Bool {
            set {
                willChangeValue(forKey: "isFinished")
                _isFinished = newValue
                didChangeValue(forKey: "isFinished")
            }
            
            get {
                return _isFinished
            }
        }

        var _isExecuting: Bool = false
        
        override var isExecuting: Bool {
            set {
                willChangeValue(forKey: "isExecuting")
                _isExecuting = newValue
                didChangeValue(forKey: "isExecuting")
            }
            
            get {
                return _isExecuting
            }
        }
        
        override func main() {
            isExecuting = true
            execute()
            isExecuting = false
            isFinished = true
        }
        
        override init() {
            super.init()
            self.qualityOfService = .background
        }
        
        func execute() {
            if isCancelled {
                return
            }
            
            print("copying records")
            if let filePath = Bundle.main.path(forResource: "citylist", ofType: ".json") {
                if let data = try? Data(contentsOf: URL(fileURLWithPath: filePath)) {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [[String: Any]] {
                            // Now let’s create an entity and new user records.
                            let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                            privateMOC.parent = managedContext
                            privateMOC.perform { [weak self] in
                                
                                let _ = self
                                let cityEntity = NSEntityDescription.entity(forEntityName: "CityMO", in: privateMOC)!
                                let date = Date()
                                for item in json {
                                    if let id = item["id"] as? Int64,
                                        let name = item["name"] as? String,
                                        let country = item["country"] as? String,
                                        let lat = (item as NSDictionary).value(forKeyPath: "coord.lat") as? Double,
                                        let lon = (item as NSDictionary).value(forKeyPath: "coord.lon")as? Double
                                    {
                                        let city = NSManagedObject(entity: cityEntity, insertInto: privateMOC)
                                        city.setValue(id, forKeyPath: "id")
                                        city.setValue(name, forKey: "name")
                                        city.setValue(country, forKeyPath: "country")
                                        city.setValue(lat, forKey: "lat")
                                        city.setValue(lon, forKey: "lon")
                                    }
                                }
                                
                                do {
                                    try privateMOC.save()
                                    try privateMOC.parent?.save()
                                    UserDefaults.standard.set(true, forKey: "DBReady")
                                    
                                    let endDate = Date()
                                    let durationFormatter = DateComponentsFormatter()
                                    durationFormatter.unitsStyle = .full
                                    durationFormatter.includesApproximationPhrase = false
                                    durationFormatter.includesTimeRemainingPhrase = false
                                    durationFormatter.allowedUnits = [.minute, .second]
                                    
                                    let duration = endDate.timeIntervalSince(date)
                                    print("done in \(durationFormatter.string(from: duration) ?? "")")
                                    print("\(duration)")
                                } catch let error {
                                    print("Saving error")
                                    print(error)
                                }
                            }

                            
                            
                            
                        }
                        
                    } catch let error {
                        print(error)
                    }
                }
            }
            
            
            if isCancelled {
                return
            }
        }
    }
}
