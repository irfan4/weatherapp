//
//  IconTypeTableViewCell.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 2019-12-21.
//  Copyright © 2019 Hamda, Inc. All rights reserved.
//

import UIKit

class IconTypeTableViewCell: UITableViewCell {

    @IBOutlet var stackView: UIStackView!
    @IBOutlet var bgView: UIView!
    @IBOutlet var selectionButton: UIButton!
    
    var indexPath: IndexPath!
    var onSelection: ((_ indexPath: IndexPath) -> Void)?
    
    private let icons: [String] = [
        "clear-day", "clear-night", "cloudy", "fog", "hail", "i-rain",
        "i-wind", "partly-cloudy-day", "partly-cloudy-night", "rain",
        "sleet", "snow", "thunderstorm", "tornado", "wind"
    ]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bgView.layer.cornerRadius = 5.0
        selectionButton.layer.cornerRadius = 5
        bgView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setIcons(folder: String, theme: String, indexPath: IndexPath, selected: Bool) {
        
        self.indexPath = indexPath
        selectionButton.isSelected = selected
        
        if theme == "dark" {
            bgView.backgroundColor = UIColor(white: 1, alpha: 0.1)
        } else {
            bgView.backgroundColor = .white
        }
        
        for icon in stackView.arrangedSubviews where icon is UIImageView {
            let iconName = "\(folder)/\(icons[icon.tag])"
            if let iconImageView = icon as? UIImageView {
                iconImageView.image = UIImage(named: iconName)
                if theme == "light" {
                    iconImageView.tintColor = UIColor(white: 0.3, alpha: 1)
                } else {
                    iconImageView.tintColor = UIColor(white: 0.7, alpha: 1)
                }
            }
        }
        //selectionButton.backgroundColor = self.bgView.backgroundColor
    }
    
    @IBAction func selectTheme(_ sender: UIButton) {
        onSelection?(self.indexPath)
    }
}
