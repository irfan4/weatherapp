//
//  WACityWeatherTableViewCell.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/16/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

class WACityWeatherTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var conditionImageView: UIImageView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var windLabel: UILabel!
    
    let currentWeatherAPI = WACurrentWeatherAPI()
    private var updatedAt: Date?
    private let dateFormatter = DateFormatter()
    var iconType = "6c"
    var forecast = DarkSkyForeCast()
    
    var onWeatherDataRefreshed: ((_ weatherItem: DarkSkyForeCast) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
    }
    
    func showTemperature(weatherItem: DarkSkyForeCast) {
        let formatter = NumberFormatter()
        formatter.allowsFloats = false
        
        var temp = "0°"
        var wind = ""
        
        temp = formatter.string(from: NSNumber(value: weatherItem.currently.temperature))!
        wind = "\(weatherItem.currently.summary)- Feels like: \(weatherItem.currently.apparentTemperature)°"
        
        self.temperatureLabel.text = "\(temp)°"
        self.windLabel.text = "\(wind)"
    }
    
    func updateIcon() {
        if let image = UIImage(named: "\(iconType)/\(forecast.currently.icon)") {
            self.conditionImageView.image = image
            self.conditionImageView.tintColor = UIColor(named: "iconColor")!
        }
    }
    
    func setContent(weatherItem: DarkSkyForeCast) {
        self.forecast = weatherItem
        // newly added city won't have any temperature value
        // shows weather information
        if let timeZone = TimeZone(identifier: weatherItem.timezone) {
            self.dateFormatter.timeZone = timeZone
        }
        self.timeLabel.text = "\(dateFormatter.string(from: Date()))"
        showTemperature(weatherItem: weatherItem)

        updateIcon()
        
        self.cityNameLabel.text = weatherItem.location
        self.activityIndicatorView.stopAnimating()
    }
    
    func updateWeather(weatherItem: DarkSkyForeCast, forceRefresh: Bool = false) {
        
        if !forceRefresh, let updatedAt = self.updatedAt {
            let lastUpdated = Date().timeIntervalSince(updatedAt)
            
            // if updated less than 15 minutes ago
            // no need for a refresh
            // set by API
            
            if (Int(lastUpdated) % 3600) / 60 < 15 {
                return
            }
        }
        
        self.temperatureLabel.isHidden = true
        self.activityIndicatorView.startAnimating()
        
        currentWeatherAPI.forecast(lat: weatherItem.latitude, lon: weatherItem.longitude, cityId: weatherItem.cityId, completion: { [weak self] forecast in
            self?.activityIndicatorView.stopAnimating()
            self?.updatedAt = Date()
            self?.temperatureLabel.isHidden = false
            forecast.location = weatherItem.location
            forecast.cityId = weatherItem.cityId
            // update content
            self?.setContent(weatherItem: weatherItem)
            
            // tell view controller data is updated
            self?.onWeatherDataRefreshed?(weatherItem)
        })
    }

}
