//
//  WAServiceBase.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/15/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

typealias SearchCompletionHandler = (_ statusCode: Int, _ cities: [WACity]) -> Void
typealias CurrentWeatherCompletionHandler = (_ statusCode: Int, _ cityWeather: WACityWeatherItem) -> Void

enum ServiceSource: String {
    case accuWeather = "TGK4beI4HH42t8dLOWDhiGWTI4Weqw8d"
    case darkSky = "2db1a691cb2d2989e067abf0fed98cee"
}

class WAServiceBase: NSObject, NSURLConnectionDelegate {
    private var baseURL: URL = URL(string: "http://dataservice.accuweather.com/")!
    private var apiKey: String = "TGK4beI4HH42t8dLOWDhiGWTI4Weqw8d"
    var urlSession: URLSession!
    var activeSource: ServiceSource = .darkSky
    
    var activeTask: URLSessionDataTask?

    override init() {
        super.init()
        
        urlSession = URLSession.shared
        apiKey = activeSource.rawValue
        switch activeSource {
        case .accuWeather:
            self.baseURL = URL(string: "http://dataservice.accuweather.com/")!
        case .darkSky:
            self.baseURL = URL(string: "https://api.darksky.net/")!
        }
    }
    
    
    func constructSearchURL(keyword: String) -> URL {
        let searchEndpoint = baseURL.appendingPathComponent("locations/v1/cities/autocomplete")
        let parameters =  [
            URLQueryItem(name: "apikey", value: apiKey),
            URLQueryItem(name: "q", value: keyword)
        ]
        
        let searchURLComponents = NSURLComponents(url: searchEndpoint, resolvingAgainstBaseURL: true)!
        
        searchURLComponents.queryItems = parameters
        return searchURLComponents.url!
    }
    
    func constructCurrentWeatherURL(url: String) -> URL {
        if self.activeSource == .darkSky {
            return self.baseURL.appendingPathComponent("forecast/\(activeSource.rawValue)/\(url)")
        }
        let endpoint = baseURL.appendingPathComponent("currentconditions/v1/\(url)")
        let parameters =  [
            URLQueryItem(name: "apikey", value: apiKey),
            URLQueryItem(name: "q", value: url),
            URLQueryItem(name: "details", value: "true")
        ]
        
        let urlComponents = NSURLComponents(url: endpoint, resolvingAgainstBaseURL: true)!
        
        urlComponents.queryItems = parameters
        return urlComponents.url!
    }
    
    func cancel() {
        activeTask?.cancel()
    }
    
}
