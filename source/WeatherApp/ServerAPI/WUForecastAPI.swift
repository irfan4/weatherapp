//
//  WUForecastAPI.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 2019-12-22.
//  Copyright © 2019 Hamda, Inc. All rights reserved.
//

import UIKit

class WUForecastAPI: NSObject {
    var activeTask: URLSessionDataTask?
    
    func daily(cityId: Int32, lat: Double, lon: Double, completion: @escaping (_ forecast: WUForecast) -> Void ) {
        let wu = URL(string: "https://api.weather.com/v3/wx/forecast/daily/5day?geocode=\(lat),\(lon)&format=json&units=s&language=en-US&apiKey=ae53f2a5a7c4442393f2a5a7c454235e")!
        
        self.activeTask = URLSession.shared.dataTask(with: wu) { (responseData: Data?, urlResponse: URLResponse?, error: Error?) in
            
            if let data = responseData {
                let forecast = WUForecast(data: data)
                forecast.saveToTemp("daily_\(cityId).dat")
                // get back to main UI thread
                DispatchQueue.main.async {
                    completion(forecast)
                }
            }
            
        }
            
        // trigger the request
        self.activeTask?.resume()
    }
}
