//
//  WAConvert.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/16/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

class WAConvert: NSObject {
    
    // Deserializes an object from the given dictionary
    class func deserialize<T>(dictionary dict: [String: Any], to: T) {
        let mirror = Mirror(reflecting: to)
        
        // loop through the properties of the class
        for property in mirror.children {
            // get dictionary pair matching the property name
            let pair = dict.filter { return $0.0 == property.label! }.first
            if let value = pair?.1 {
                // try to convert the dictionary type to class property type
                convert(object: to, propertyName: property.label!, value: value, toType: property.value)
            }
        }
        
    }
    
    /// converts the object strictly, if type does not match it will crash
    class func convert<T>(object: T, propertyName: String, value: Any?, toType: Any?)  {
        // if nil, abort right away
        if value == nil { return }
        
        // if convertion type is not supported bailout
        if toType == nil { return }
        
        
        // the class name as string
        let clazz = swiftStringFromClass(toType as! NSObject)
        
        // default type should be assigned as it is
        if clazz == "__NSCFConstantString" ||
            clazz == "__NSCFString" ||
            clazz == "__NSCFNumber" {
            // TODO: Crash free mapping :)
            // Instead of directly assigning value to key, check if value datatype matches destination datatype
            (object as! NSObject).setValue(value, forKey: propertyName)
        } else { // if it's a WAObject class try parsing
            if let waObject = toType as? WAObject {
                waObject.perform(#selector(WAObject.init(withDictionary:)), with: value as! [String: Any])
            }
        }
    }
    
    // returns the string representation of the class type
    class func swiftStringFromClass(_ theObject: NSObject) -> String {
        return NSStringFromClass(type(of: theObject))
    }
    
    // converts any Encodable class to json string or nil if it fails
    class func toJSON<T: Encodable>(waObject: T) -> String? {
        if let data = try? JSONEncoder().encode(waObject) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
}
