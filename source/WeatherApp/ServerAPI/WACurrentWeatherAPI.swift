//
//  WACurrentWeatherAPI.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/16/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

class WACurrentWeatherAPI: WAServiceBase {
    func forecast(lat: Double, lon: Double, cityId: Int32, completion: @escaping (_ forecast: DarkSkyForeCast) -> Void ) {
        let darkSky = URL(string: "https://api.darksky.net/forecast/2db1a691cb2d2989e067abf0fed98cee/\(lat),\(lon)?units=uk2")!
        
        if FileManager.default.fileExists(atPath: "\(NSTemporaryDirectory())\(cityId).dat") {
            let forecast = DarkSkyForeCast(fileNameInTemp: "\(cityId).dat")
            // get back to main UI thread
            DispatchQueue.main.async {
                completion(forecast)
            }
            return
        }
        
        self.activeTask = self.urlSession.dataTask(with: darkSky) { (responseData: Data?, urlResponse: URLResponse?, error: Error?) in
            
            if let data = responseData {
                let forecast = DarkSkyForeCast(data: data)
                forecast.saveToTemp("\(cityId).dat")
                // get back to main UI thread
                DispatchQueue.main.async {
                    completion(forecast)
                }
            }
            
        }
            
        // trigger the request
        self.activeTask?.resume()
    }
    
    func forecast(city: City, completion: @escaping (_ forecast: DarkSkyForeCast) -> Void ) {
        forecast(lat: city.lat, lon: city.lon, cityId: city.id, completion: completion)
    }
    
    func current(city: WACity, completion: @escaping CurrentWeatherCompletionHandler) {
        let currentWeatherURL = constructCurrentWeatherURL(url: city.id)
        // prepare the data task
        self.activeTask = self.urlSession.dataTask(with: currentWeatherURL) { (responseData: Data?, urlResponse: URLResponse?, error: Error?) in
            
            // Default is zero, if network fails or times out we would know
            // otherwise standard httpStatusCode
            var statusCode = 0
            var weatherItem = WACityWeatherItem()
            
            defer {
                // get back to main UI thread
                DispatchQueue.main.async {
                    completion(statusCode, weatherItem)
                }
            }
            
            if let response = urlResponse as? HTTPURLResponse {
                statusCode = response.statusCode
            }
            
            if let data = responseData {
                do {
                    let decoder = JSONDecoder()
                    let items = try decoder.decode([WACityWeatherItem].self, from: data)
                    if let first = items.first {
                        weatherItem = first
                    }
                    
                } catch let error {
                    print(error)
                    // override status code
                    // no content - Failed serialzation
                    statusCode = 204
                }
            }
        }
        
        // trigger the request
        self.activeTask?.resume()
    }
}
