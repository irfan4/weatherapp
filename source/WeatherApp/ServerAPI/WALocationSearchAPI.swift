//
//  WALocationSearchAPI.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/15/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

class WALocationSearchAPI: WAServiceBase {
    
    
    func search(keyword: String,
                completion: @escaping SearchCompletionHandler) {
        // ignore less than 3 characters, because API doesn't support less than three characters for searching
        if keyword.count < 3 {
            // No need to call the completion handler
            // API was not called
            return
        }
        // cancel the task if a new task comes in
        cancel()
        
        let searchURL = constructSearchURL(keyword: keyword)
        
        // prepare the data task
        self.activeTask = self.urlSession.dataTask(with: searchURL) { (responseData: Data?, urlResponse: URLResponse?, error: Error?) in
            
            // Default is zero, if network fails or times out we would know
            // otherwise standard httpStatusCode
            var statusCode = 0
            var cities = [WACity]()
            
            defer {
                // get back to main UI thread
                DispatchQueue.main.async {
                    completion(statusCode, cities)
                }
            }
            
            if let response = urlResponse as? HTTPURLResponse {
                statusCode = response.statusCode
            }
            
            if let data = responseData {
                do {
                    // Returned response would be an array of cities
                    let decoder = JSONDecoder()
                    cities = try decoder.decode([WACity].self, from: data)
                } catch let error {
                    print(error)
                    // override status code
                    // no content - Failed serialzation
                    statusCode = 204
                }
            }
        }
        
        // trigger the request
        self.activeTask?.resume()
    }
}
