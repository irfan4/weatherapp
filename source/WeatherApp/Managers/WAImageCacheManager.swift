//
//  WAImageCacheManager.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/17/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

class WAImageCacheManager: NSObject {
    static let shared: WAImageCacheManager = WAImageCacheManager()
    
    let imageCache = NSCache<NSString, UIImage>()
    
    private override init() {
        super.init()
    }
    
    // Check if cache has image, otherwise download and stores in cache
    func downloadImage(url: URL, completion: @escaping (_ image: UIImage?, _ error: Error? ) -> Void) {
        
        // looks for cached image
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            // returns image in completion handler
            completion(cachedImage, nil)
        } else {
            // download the image
            downloadData(url: url) { data, response, error in
                if let error = error {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                } else if let data = data, let image = UIImage(data: data) {
                    // saves to cache
                    self.imageCache.setObject(image, forKey: url.absoluteString as NSString)
                    
                    // returns image in completion handler
                    DispatchQueue.main.async {
                        completion(image, nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        completion(nil, NSError(domain: url.absoluteString, code: 400, userInfo: nil))
                    }
                }
            }
        }
    }
    
    // Simple URL Download as Data
    private func downloadData(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        // Download data Task
        let request = URLRequest(url: url)
        if let cached = URLCache.shared.cachedResponse(for: request) {
            completion(cached.data, cached.response, nil)
            return
        }
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let response = response, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                let cachedURLResponse = CachedURLResponse(response: response, data: data!)
                URLCache.shared.storeCachedResponse(cachedURLResponse, for: request)
            }
            completion(data, response, error)
        }.resume()
    }
}
