//
//  WeatherViewController.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 2019-12-24.
//  Copyright © 2019 Hamda, Inc. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {
    
    var forecast: DarkSkyForeCast = DarkSkyForeCast()
    
    private let TopView_Min_Height: CGFloat = 150
    private let TopView_Max_Height: CGFloat = 400
    
    @IBOutlet var topView: UIView!
    @IBOutlet var cityNameLabel: UILabel!
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var currentWeatherStack: UIStackView!
    @IBOutlet var currentWeatherTopView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTopView()
    }
    
    func setupTopView() {
        topView.translatesAutoresizingMaskIntoConstraints = true
        topView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: TopView_Max_Height)
        
        if let city = DatabaseManager.shared.getCity(withId: forecast.cityId) {
            countryLabel.text = city.country
            cityNameLabel.text = city.name
            let df = DateFormatter()
            df.dateFormat = "E, MMM d"
            if forecast.timezone == TimeZone.current.identifier {
            } else if let tz = TimeZone(identifier: forecast.timezone) {
                df.timeZone = tz
                df.dateFormat = "\(df.dateFormat!) HH:mm"
            }
            dateLabel.text = df.string(from: Date())
        }
        
        countryLabel.sizeToFit()
        dateLabel.sizeToFit()
        cityNameLabel.sizeToFit()
        
        topViewResizedTo(percent: 1.0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    @IBAction func test(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func setFrame(x: CGFloat, on v: UIView) {
        var f = v.frame
        f.origin.x = x
        v.frame = f
    }

    func topViewResizedTo(percent: CGFloat) {
        let f = topView.bounds
        let c = (f.width / 2)
        let Left_MIN: CGFloat = 25
        
        let CitySV_MAX: CGFloat = c - (cityNameLabel.frame.width / 2)
        let CitySV = CitySV_MAX * percent
        setFrame(x: max(CitySV, Left_MIN), on: cityNameLabel)
        
        
        let Country_MAX: CGFloat = c - (countryLabel.frame.width / 2)
        let CountryX = Country_MAX * percent
        setFrame(x: max(CountryX, Left_MIN), on: countryLabel)
        
        let Date_MAX: CGFloat = c - (dateLabel.frame.width / 2)
        let DateX = Date_MAX * percent
        setFrame(x: max(DateX, Left_MIN), on: dateLabel)
        
        let CU_TOPView_MIN: CGFloat = f.width - currentWeatherTopView.bounds.width
        let CU_TOPView_MAX: CGFloat = f.width
        let CU_TopView_X = CU_TOPView_MAX * percent
        setFrame(x: max(CU_TopView_X, CU_TOPView_MIN), on: currentWeatherTopView)
        
        
        // current weather stack
        currentWeatherStack.alpha = percent
        if percent < 0.5 {
            currentWeatherStack.alpha = percent - 0.2
        }
    }
}

extension WeatherViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y > 0 {
            let h = max(TopView_Min_Height, 400 - scrollView.contentOffset.y)
            topView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: h)
        } else if scrollView.contentOffset.y < 0 {
            let h = 400 - scrollView.contentOffset.y
            topView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: h)
        }
        
        let w = topView.frame.height - TopView_Min_Height
        let h = TopView_Max_Height - TopView_Min_Height
        let p = w / h
        topViewResizedTo(percent: p)
        self.view.needsUpdateConstraints()
    }
}
