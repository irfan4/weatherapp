//
//  WARootViewController.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/15/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

class WARootViewController: WAViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var celsiusButton: UIButton!
    let refreshControl = UIRefreshControl()
    @IBOutlet weak var fahrenheitButton: UIButton!
    
    var cities = [DarkSkyForeCast]()
    var iconType = "6c"
    // tracks if you made any change to the dataset
    // if there is any change we will save it, otherwise we won't
    var isUserCitiesUpdated: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addCity(_:)))
        
        // set the refresh controller
        tableView.refreshControl = refreshControl
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        
        // setup scale conversion buttons
        setupScaleButtons()
        
        loadUserCities()
        
        if self.cities.count == 0 {
            tableView.isHidden = true
        } else {
            tableView.isHidden = false
        }
        if let iconType = UserDefaults.standard.string(forKey: "theme") {
            self.iconType = iconType
        }
    }
    
    func changeTheme(newTheme: String) {
        self.iconType = newTheme
        for cell in tableView.visibleCells {
            if let wCell = cell as? WACityWeatherTableViewCell {
                wCell.iconType = self.iconType
                wCell.updateIcon()
            }
        }
    }
    
    func setupScaleButtons(shouldUpdateTableView: Bool = false) {
        if UserDefaults.standard.integer(forKey: "scale") == 0 {
            celsiusButton.alpha = 1.0
            fahrenheitButton.alpha = 0.5
        } else {
            celsiusButton.alpha = 0.5
            fahrenheitButton.alpha = 1.0
        }
        
        if shouldUpdateTableView {
            for i in 0..<cities.count {
                let indexPath = IndexPath(item: i, section: 0)
                if let cell = tableView.cellForRow(at: indexPath) as? WACityWeatherTableViewCell {
                    cell.showTemperature(weatherItem: self.cities[indexPath.row])
                }
            }
        }
    }
    
    // MARK:- Loads & Retrive User cities from disk
    func loadUserCities() {
        self.cities = DatabaseManager.shared.my()
//        let api = WUForecastAPI()
//        let city = self.cities[0]
//        api.daily(cityId: city.cityId, lat: city.latitude, lon: city.longitude) { forecast in
//            
//            print(forecast)
//        }
    }
    
    // MARK:- Completion Handler
    // Completion Handler for LocationSearch ViewComtroller
    // Gets called when user taps on a city table row
    func shouldAddCity(city: City, forecast: DarkSkyForeCast) {
        DatabaseManager.shared.add(city: city, initialForecast: forecast)

        // Add to dataset and display
        self.cities.append(forecast)
        self.tableView.isHidden = false
        
        let indexPath = IndexPath(item: self.cities.count - 1, section: 0)
        self.tableView.insertRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        
        // if there are many items, scroll so user should know the city is added to list
        self.tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
    }
    
    // Weather Data Persistance & Updation
    func onWeatherDataRefreshed(cityWeatherItem: DarkSkyForeCast) {
        // the array is already updated
        // just go ahead and save it
        self.isUserCitiesUpdated = true
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? WALocationSearchViewController {
            vc.onAddCityRequest = shouldAddCity
        } else if let vc = segue.destination as? SettingsTableViewController {
            vc.onChangeTheme = changeTheme
        } else if let vc = segue.destination as? WeatherViewController {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: indexPath, animated: true)
                let forecast = self.cities[indexPath.row]
                vc.forecast = forecast
            }
        }
    }
    
    // MARK:- UITableViewDatasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCellIdentifier", for: indexPath) as! WACityWeatherTableViewCell
        cell.iconType = self.iconType
        let forecast = cities[indexPath.row]
        // Set content
        cell.setContent(weatherItem: forecast)
        // get update
        cell.updateWeather(weatherItem: forecast, forceRefresh: false)
        // set completion handler
        cell.onWeatherDataRefreshed = onWeatherDataRefreshed
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // MARK:- UITableView Delegates
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            let city = self.cities[indexPath.row]
            // delete item at indexPath
            self.cities.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            
            // now if we don't have any items left reset the whole view
            if self.cities.count == 0 {
                // Wait for the remove animation to finish before we hide the table
                self.delay(0.365) {
                    self.tableView.isHidden = true
                    self.tableView.isEditing = false
                }
            }
            
            // save the new dataset
            DatabaseManager.shared.remove(forecast: city)
        }
        
        
        return [delete]
        
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.cities[sourceIndexPath.row]
        self.cities.remove(at: sourceIndexPath.row)
        self.cities.insert(movedObject, at: destinationIndexPath.row)
        
        // To enable save user cities
        self.isUserCitiesUpdated = true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // show multiple delete button
        if self.isEditing {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.trash, target: self, action: #selector(removeCities(_:)))
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        // if no row is selected remove the trash button fron navigation
        if tableView.indexPathsForSelectedRows == nil {
            self.navigationItem.leftBarButtonItem = nil
        }
    }
    
    // MARK:- Actions
    @objc func addCity(_ sender: Any) {
        self.performSegue(withIdentifier: "addCity", sender: nil)
    }
    @objc func showSettings(_ sender: Any) {
        self.performSegue(withIdentifier: "showSettings", sender: nil)
    }
    
    @objc func refreshWeatherData(_ sender: Any) {
        self.refreshControl.endRefreshing()
        
        for i in 0..<cities.count {
            let indexPath = IndexPath(item: i, section: 0)
            if let cell = tableView.cellForRow(at: indexPath) as? WACityWeatherTableViewCell {
                cell.updateWeather(weatherItem: self.cities[indexPath.row], forceRefresh: true)
            }
        }
    }
    
    @IBAction func toggleEdit(_ sender: Any) {
        let editing = !self.tableView.isEditing
        self.tableView.setEditing(editing, animated: true)
        
        if !editing {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addCity(_:)))
        } else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "gear"), style: .done, target: self, action: #selector(showSettings(_:)))
        }
    }
    
    @IBAction func removeCities(_ sender: Any) {
        if let indexPaths = tableView.indexPathsForSelectedRows {
            for indexPath in indexPaths {
                DatabaseManager.shared.remove(forecast: cities[indexPath.row])
            }
            // remove items from array
            self.cities.remove(at: indexPaths.map { $0.row })
            
            // show user the rows have been removed
            self.tableView.deleteRows(at: indexPaths, with: UITableView.RowAnimation.automatic)
            
            // remove the trash button from navigation
            self.navigationItem.leftBarButtonItem = nil

            // now if we don't have any items left reset the whole view
            if cities.count == 0 {
                // Wait for the remove animation to finish before we hide the table
                self.delay(0.365) {
                    self.tableView.isHidden = true
                    self.tableView.isEditing = false
                }
            }
            
            // save the new dataset
            isUserCitiesUpdated = true
            self.tableView.isEditing = false
        }
    }
    
    @IBAction func switchToFahrenheit(_ sender: Any) {
        UserDefaults.standard.set(1, forKey: "scale")
        UserDefaults.standard.synchronize()
        
        setupScaleButtons(shouldUpdateTableView: true)
    }
    
    @IBAction func switchToCelsius(_ sender: Any) {
        UserDefaults.standard.set(0, forKey: "scale")
        UserDefaults.standard.synchronize()
        
        setupScaleButtons(shouldUpdateTableView: true)
    }
    
}
