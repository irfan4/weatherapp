//
//  WALocationSearchViewController.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/15/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

typealias AddCityRequestHandler = (_ city: City, _ forecast: DarkSkyForeCast) -> Void

class WALocationSearchViewController: WAViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    let searchAPI: WALocationSearchAPI = WALocationSearchAPI()
    let forecastAPI: WACurrentWeatherAPI = WACurrentWeatherAPI()
    var cities = [City]()
    
    var onAddCityRequest: AddCityRequestHandler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIBarButtonItem.appearance(whenContainedInInstancesOf:[UISearchBar.self]).tintColor = UIColor.systemBlue
        
        let searchTextFieldAppearance = UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self])
        searchTextFieldAppearance.textColor = UIColor.systemBlue
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // activate search bar input
        searchBar.becomeFirstResponder()
    }
    
    // MARK:- UISearchBar Delegates
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            // reset array & reload tableview
            self.cities = [City]()
            self.tableView.reloadData()
        } else {
            DatabaseManager.shared.search(keyword: searchText) { [weak self] cities in
                if let strongSelf = self {
                    strongSelf.cities = cities
                    strongSelf.tableView.reloadData()
                }
            }
        }
    }
    
    // MARK:- UITableViewDatasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityCellIdentifier", for: indexPath)
        
        let city = cities[indexPath.row];
        cell.textLabel?.text = city.name
        
        return cell
    }
    
    // MARK:- UITableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city = self.cities[indexPath.row]
        forecastAPI.forecast(city: city) { [weak self] (forecast) in
            forecast.cityId = city.id
            forecast.location = city.name
            self?.onAddCityRequest?(city, forecast)
            self?.dismiss(animated: true, completion: nil)
        }
    }
}
