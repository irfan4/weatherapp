//
//  SettingsTableViewController.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 2019-12-21.
//  Copyright © 2019 Hamda, Inc. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    private let light: [String] = [
        "1m",
        "2m",
        "3m",
        "6c"
    ]
    private let dark: [String] = [
        "1c",
        "1m",
        "2c",
        "2m",
        "3c",
        "3m",
        "4c",
        "4m",
        "5c",
        "5m",
        "6c"
    ]
    
    var current: String = "6c"
    var onChangeTheme: ((_ newTheme: String) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let iconType = UserDefaults.standard.string(forKey: "theme") {
            self.current = iconType
        }
    }
    
    func changeTheme(indexPath: IndexPath) {
        if indexPath.section == 0 {
            current = light[indexPath.row]
        } else {
            current = dark[indexPath.row]
        }
        UserDefaults.standard.set(current, forKey: "theme")
        UserDefaults.standard.synchronize()
        self.tableView.reloadData()
        onChangeTheme?(current)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return light.count
        }
        return dark.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! IconTypeTableViewCell
        if indexPath.section == 0 {
            cell.setIcons(folder: light[indexPath.row], theme: "light", indexPath: indexPath, selected: current == light[indexPath.row])
        } else {
            cell.setIcons(folder: dark[indexPath.row], theme: "dark", indexPath: indexPath, selected: current == dark[indexPath.row])
        }
        cell.onSelection = changeTheme
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Light"
        } else {
            return "Dark"
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textColor = .white
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func save(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
