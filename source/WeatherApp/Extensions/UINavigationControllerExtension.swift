//
//  WANavigationController.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/15/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

extension UINavigationController {
    override open var prefersStatusBarHidden: Bool {
        get {
            if let vc = self.viewControllers.last {
                return vc.prefersStatusBarHidden
            }
            
            return false
        }
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            if let vc = self.viewControllers.last {
                return vc.preferredStatusBarStyle
            }
            
            return .default
        }
    }
}
