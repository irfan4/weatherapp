//
//  ArrayExtension.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/17/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import Foundation

extension Array {
    
    // removes multiple indexes
    // always start from larger index
    // this is for non-sequential indexes
    mutating func remove(at indexes: [Int]) {
        //TODO: Remove duplicates from indexes input array :)
        for index in indexes.sorted(by: >) {
            remove(at: index)
        }
    }
}
