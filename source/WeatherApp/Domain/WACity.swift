//
//  WACity.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/16/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

class WACity: Codable {
    var id: String = ""
    var name: String = ""
    var version: Int = 1
    var type: String = ""
    var country: Country = Country()
    var adminArea: Country = Country()
    var rank: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case id = "Key"
        case version = "Version"
        case type = "Type"
        case rank = "Rank"
        case name = "LocalizedName"
        case country = "Country"
        case adminArea = "AdministrativeArea"
    }
    
}

class Country: Codable {
    var id: String = ""
    var name: String = ""
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "LocalizedName"
    }
}
