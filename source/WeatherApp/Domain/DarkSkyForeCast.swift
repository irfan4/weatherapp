//
//  DarkSkyForeCast.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 2019-12-20.
//  Copyright © 2019 Hamda, Inc. All rights reserved.
//

import EVReflection

class DarkSkyForeCast: EVObject {
    var cityId: Int32 = 0
    var latitude: Double = 0
    var longitude: Double = 0
    var location: String = ""
    var timezone: String = ""
    var currently: DarkSkyMeasurement = DarkSkyMeasurement()
    var hourly = DarkSkyTimedInfo()
    var daily = DarkSkyTimedInfo()
    var alerts: [DarkSkyAlert] = [DarkSkyAlert]()
    var flags: DarkSkyFlags = DarkSkyFlags()
    var offset: Int = 0
}

class DarkSkyTimedInfo: EVObject {
    var summary: String = ""
    var icon: String = ""
    var data: [DarkSkyMeasurement] = [DarkSkyMeasurement]()
}

class DarkSkyMeasurement: EVObject {
    var time: TimeInterval = 0
    var summary: String = ""
    var icon: String = ""
    var precipIntensity: Float = 0
    var precipProbability: Float = 0
    var precipType: String = ""
    var temperature: Float = 0
    var apparentTemperature: Float = 0
    var dewPoint: Float = 0
    var humidity: Float = 0
    var pressure: Float = 0
    var windSpeed: Float = 0
    var windGust: Float = 0
    var windBearing: Float = 0
    var windGustTime: TimeInterval = 0
    var cloudCover: Float = 0
    var uvIndex: Float = 0
    var visibility: Float = 0
    var ozone: Float = 0
    var temperatureLowTime: Float = 0
    var temperatureMax: Float = 0
    var precipIntensityMax: Float = 0
    var apparentTemperatureMinTime: TimeInterval = 0
    var precipIntensityMaxTime: TimeInterval = 0
    var sunriseTime: TimeInterval = 0
    var sunsetTime: TimeInterval = 0
    var apparentTemperatureHighTime: TimeInterval = 0
    var apparentTemperatureLowTime: TimeInterval = 0
    var temperatureHighTime: TimeInterval = 0
    var temperatureMinTime: TimeInterval = 0
    var uvIndexTime: TimeInterval = 0
    var apparentTemperatureMaxTime: TimeInterval = 0
    var temperatureMaxTime: TimeInterval = 0
    var apparentTemperatureMin: Float = 0
    var apparentTemperatureHigh: Float = 0
    var temperatureLow: Float = 0
    var temperatureHigh: Float = 0
    var apparentTemperatureMax: Float = 0
    var moonPhase: Float = 0
    var apparentTemperatureLow: Float = 0
    var temperatureMin: Float = 0
    var precipAccumulation: Float = 0
}

class DarkSkyAlert: EVObject {
    var title: String = ""
    var regions: [String] = [String]()
    var severity: String = ""
    var time: TimeInterval = 0
    var expires: TimeInterval = 0
    var desc: String = ""
    var uri: String = ""
    
    override func propertyMapping() -> [(keyInObject: String?, keyInResource: String?)] {
        return [
            ("desc", "description")
        ]
    }
}

class DarkSkyFlags: EVObject {
    var meteoalarmLicense: String = ""
    var nearestStation: Float = 0
    var units: String = ""
    var sources: [String] = [String]()
    
    override func propertyMapping() -> [(keyInObject: String?, keyInResource: String?)] {
        return [
            ("meteoalarmLicense", "meteoalarm-license"),
            ("nearestStation", "nearest-station")
        ]
    }
}
