//
//  WACityWeatherItem.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/16/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

class MyCity: Codable {
    var location = WACity()
    var weather = WACityWeatherItem()
    
    enum CodingKeys: String, CodingKey {
        case location
        case weather
    }
}

class WACityWeatherItem: Codable {
    var localObservationDateTime: String = ""
    var epochTime: Int64 = 0
    var weatherText: String = ""
    var weatherIcon: Int?
    var hasPrecipitation: Bool = false
    var isDayTime: Bool = false
    var temperature: Measurement = Measurement()
    var real: Measurement = Measurement()
    var wind: Wind = Wind()
    
    enum CodingKeys: String, CodingKey {
        case localObservationDateTime = "LocalObservationDateTime"
        case epochTime = "EpochTime"
        case weatherText = "WeatherText"
        case weatherIcon = "WeatherIcon"
        case hasPrecipitation = "HasPrecipitation"
        case isDayTime = "IsDayTime"
        case temperature = "Temperature"
        case real = "RealFeelTemperature"
        case wind = "Wind"
    }
}

class Unit: Codable {
    var value: Float = 0.0
    var symbol: String = ""
    var type: Int = 0
    enum CodingKeys: String, CodingKey {
        case value = "Value"
        case symbol = "Unit"
        case type = "UnitType"
    }
}

class Measurement: Codable {
    var metric: Unit = Unit()
    var imperial: Unit = Unit()
    
    enum CodingKeys: String, CodingKey {
        case metric = "Metric"
        case imperial = "Imperial"
    }
}

class Wind: Codable {
    var speed = Measurement()
    var direction = Direction()
    
    enum CodingKeys: String, CodingKey {
        case speed = "Speed"
        case direction = "Direction"
    }
}

class Direction: Codable {
    var degrees: Int = 0
    var localized: String = ""
    var english: String = ""
    
    enum CodingKeys: String, CodingKey {
        case degrees = "Degrees"
        case localized = "Localized"
        case english = "English"
    }
}
