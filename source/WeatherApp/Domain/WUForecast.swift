//
//  WUForecast.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 2019-12-22.
//  Copyright © 2019 Hamda, Inc. All rights reserved.
//

import EVReflection

class WUForecast: EVObject {
    var dayOfWeek = [String]()
    var expirationTimeUtc = [TimeInterval]()
    var moonPhase = [String]()
    var moonPhaseCode = [String]()
    var moonPhaseDay = [Int]()
    var moonriseTimeLocal = [String]()
    var moonriseTimeUtc = [TimeInterval]()
    var moonsetTimeLocal = [String]()
    var moonsetTimeUtc = [TimeInterval]()
    var narrative = [String]()
    var qpf = [NSNumber]()
    var qpfSnow = [NSNumber]()
    var sunriseTimeLocal = [String]()
    var sunriseTimeUtc = [TimeInterval]()
    var sunsetTimeLocal = [String]()
    var sunsetTimeUtc = [TimeInterval]()
    var temperatureMax = [String]()
    var temperatureMin = [String]()
    var validTimeLocal = [String]()
    var validTimeUtc = [TimeInterval]()
    
    override func propertyConverters() -> [(key: String, decodeConverter: ((Any?) -> ()), encodeConverter: (() -> Any?))] {
        return [
            (
                "temperatureMin",
                { (value: Any?) in
                    if let n = value as? [NSNumber?] {
                        self.temperatureMin = n.map { $0?.stringValue ?? "" }
                    }
                },
                { () in
                    return self.temperatureMin
            }),
            (
                "temperatureMax",
                { (value: Any?) in
                    if let n = value as? [NSNumber?] {
                        self.temperatureMax = n.map { $0?.stringValue ?? "" }
                    }
                },
                { () in
                    return self.temperatureMax
            })
        ]
    }
}
