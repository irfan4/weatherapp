//
//  WACondition.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/16/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

class WACondition: WAObject, Codable {
    var text: String = ""
    var icon: String = ""
    var code: Int = 0
}
