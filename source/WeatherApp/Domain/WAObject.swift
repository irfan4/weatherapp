//
//  WAObject.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/16/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

@objcMembers
class WAObject: NSObject {
    override init() {
        super.init()
    }
    
    // MARK:- Initializes from a dictionary
    @objc convenience init(withDictionary dict: [String: Any]) {
        self.init()

        WAConvert.deserialize(dictionary: dict, to: self)
    }
}
