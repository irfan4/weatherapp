//
//  WACurrentWeather.swift
//  WeatherApp
//
//  Created by Irfan Bashir on 11/16/18.
//  Copyright © 2018 Hamda, Inc. All rights reserved.
//

import UIKit

class WACurrentWeather: WAObject, Codable {
    var last_updated_epoch: Double = 0.0
    var last_updated: String = ""
    var temp_c: Float = 0.0
    var temp_f: Float = 0.0
    var is_day: Int = 1
    var wind_mph: Float = 0.0
    var wind_kph: Float = 0.0
    var wind_degree: Float = 0.0
    var wind_dir: String = ""
    var pressure_mb: Float = 0.0
    var pressure_in: Float = 0.0
    var precip_mm: Float = 0.0
    var precip_in: Float = 0.0
    var humidity: Float = 0.0
    var cloud: Int = 0
    var feelslike_c: Float = 0.0
    var feelslike_f: Float = 0.0
    var vis_km: Float = 0.0
    var vis_miles: Float = 0.0
    var uv: Float = 0.0
    
    var condition: WACondition = WACondition()
}
